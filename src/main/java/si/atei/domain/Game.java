package si.atei.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Game implements Serializable {

    private Wheel wheel = new Wheel();
    private List<Player> players = new ArrayList<>();

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(String name, int money) {
        Player p = new Player();
        p.setMoney(money);
        p.setName(name);
        p.setGame(this);
        players.add(p);
    }

    /**
     * Play a round of roulette.
     * <p>
     * Spin the wheel and process all players' bets
     */
    public void playRound() {
        wheel.spin();
        players.forEach(Player::playRound);
    }

    /**
     * Number on the wheel
     *
     * @return
     */
    public int number() {
        return wheel.getResult();
    }

}
