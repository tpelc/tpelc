package si.atei.domain;

public class OddEvenBet extends Bet {

    private boolean even;

    protected OddEvenBet(int money, boolean even) {
        super(money);
        this.even = even;
    }

    @Override
    protected int odds() {
        return 2;
    }

    @Override
    protected boolean won(int number) {
        if (number == 0) {
            // House wins for zero
            return false;
        } else {
            return (number % 2 == 0) == even;
        }
    }

    @Override
    public String toString() {
        return super.toString() + " on " + (even ? "even" : "odd");
    }
}
