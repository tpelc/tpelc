package si.atei.web;

import org.apache.wicket.markup.repeater.util.ModelIteratorAdapter;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import si.atei.domain.Player;

public final class DefaultModelIterator extends ModelIteratorAdapter<Player> {
    DefaultModelIterator(Iterable<Player> iterable) {
        super(iterable);
    }

    @Override
    protected IModel<Player> model(Player object) {
        return Model.of(object);
    }
}